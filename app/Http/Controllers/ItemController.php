<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use DB;
use Validator;
use PDF;

class ItemController extends Controller
{
    public function __construct(Request $request){
        $this->param = $this->checkToken($request);
        $this->request = $request;
    }

    public function getItem(request $request){
        $input = json_decode($this->request->getContent(),true);
        $rules = [
          'CategoryID' => 'required'
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $CategoryID = $input['CategoryID'];

            $result = DB::table('Item')
            ->leftjoin('Category','Item.CategoryID','=','Category.CategoryID')
            ->select(['ItemID','ItemCode','ItemName','Item.Description','Item.CategoryID','CategoryName'])
            ->where('Item.Status',null)
            ->where('Item.CategoryID',$CategoryID)
            ->orderby('ItemID','desc')
            ->get();
        

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Item' => $result
      );

       return Response()->json($endresult);
    }

    public function getItemDetail(Request $request){

      $input = json_decode($this->request->getContent(),true);
      $rules = [
        'ItemID' => 'required'
      ];

      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $ItemID = $input['ItemID'];
      $result = DB::table('Item')
      ->select(['ItemID','ItemCode','ItemName','Description'])
      ->where('ItemID',$ItemID)
      ->get();
      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Item' => $result
      );
      return Response()->json($endresult);
}

public function insertUpdateItem(Request $request){
    $input = json_decode($request->getContent(),true);
    $rules = [
        'ItemCode' => 'required',
        'ItemName' => 'required'
    ];

    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $errors = $validator->errors();
        $errorList = $this->checkErrors($rules, $errors);
        $additional = null;
        $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
        return response()->json($response);
    }
    $ID = @$input['ItemID'];
    $unique = array(
        'Table' => "Item",
        'ID' => $ID,
        'Column' => "ItemName",
        'String' => $input['ItemName']
    );
    $uniqueItemName = $this->unique($unique);
    $unique['Column'] = "ItemCode";
    $unique['String'] = $input['ItemCode'];
    $uniqueEmail = $this->unique($unique);
    $param = array(
        'ItemCode' => $input['ItemCode'],
        'ItemName' => $input['ItemName'],
        'Description' => @$input['Description']);

      if ($ID == null){
        $result = DB::table('Item')->insert($param);
        $ID = $this->getLastVal();
        }
      else{$result = DB::table('Item')->where('ItemID',$ID)->update($param);}

          $result = $this->checkReturn($result);
          return Response()->json($result);

  }

  public function DeleteItem(Request $request){
       $input = json_decode($this->request->getContent(),true);
       $rules = [
         'ItemID' => 'required'
       ];

       $validator = Validator::make($input, $rules);
       if ($validator->fails()) {
           $errors = $validator->errors();
           $errorList = $this->checkErrors($rules, $errors);
           $additional = null;
           $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
           return response()->json($response);
       }
       $ItemID = @$input['ItemID'];
       $param = array('Status' => 'D','Archived' => now());
       $result = DB::table('Item')->where('ItemID', $ItemID)->update($param);

      $result = $this->checkReturn($result);

      return Response()->json($result);

  }

}
