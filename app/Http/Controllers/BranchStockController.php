<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use DB;
use Validator;
use PDF;

class BranchStockController extends Controller
{
    public function __construct(Request $request){
        $this->param = $this->checkToken($request);
        $this->request = $request;
    }

    public function getBranchStock(){

      $result = DB::table('BranchStock')
      ->leftjoin('Item','Item.ItemID','=','BranchStock.ItemID')
      ->leftjoin('Category','Item.CategoryID','=','Category.CategoryID')
      ->select(['BranchStockID','BranchStock.ItemID','ItemCode','ItemName','Item.Description','Item.CategoryID','CategoryName','BranchStock.Quantity'])
      ->orderby('BranchStock.ItemID','desc')
      ->get();

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Item' => $result,
          'Data' => $this->param
      );


       return Response()->json($endresult);
    }

    public function getBranchStockDetail(Request $request){

      $input = json_decode($this->request->getContent(),true);
      $rules = [
        'BranchStockID' => 'required'
      ];

      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $BranchStockID = $input['BranchStockID'];
      $result = DB::table('BranchStock')
      ->leftjoin('Item','Item.ItemID','=','BranchStock.ItemID')
      ->leftjoin('Category','Item.CategoryID','=','Category.CategoryID')
      ->select(['BranchStockID','BranchStock.ItemID','ItemCode','ItemName','Item.Description','Item.CategoryID','CategoryName','BranchStock.Quantity'])
      ->where('BranchStockID',$BranchStockID)
      ->orderby('BranchStock.ItemID','desc')
      ->get();
      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Item' => $result
      );
      return Response()->json($endresult);
}

public function insertUpdateBranchStock(Request $request){
    $input = json_decode($request->getContent(),true);
    $rules = [
        'ItemID' => 'required',
        'BranchID' => 'required',
        'Quantity' => 'required|numeric'
    ];

    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $errors = $validator->errors();
        $errorList = $this->checkErrors($rules, $errors);
        $additional = null;
        $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
        return response()->json($response);
    }
    $ItemID = $input['ItemID'];
    $BranchID = $input['BranchID'];

    $param = array(
        'ItemID' => $input['ItemID'],
        'BranchID' => $input['BranchID'],
        'Quantity' => @$input['Quantity']);
        //harus cek dlu di table stock, udah ada item itu belum.
    $check = DB::table('BranchStock')
    ->where('ItemID',$ItemID)
    ->where('BranchID',$BranchID)
    ->count();

      if ($check == 0){
        $result = DB::table('BranchStock')->insert($param);
        $ID = $this->getLastVal();
        }
      else{
          $stock = DB::table('BranchStock')
          ->where('ItemID',$ItemID)
          ->where('BranchID',$BranchID)
          ->first()->Quantity;

          $result = DB::table('BranchStock')
          ->where('ItemID',$ItemID)
          ->where('BranchID',$BranchID)
          ->update(array('Quantity' => $param['Quantity']+$stock));}

          $result = $this->checkReturn($result);
          return Response()->json($result);

  }

  public function DeleteItem(Request $request){
       $input = json_decode($this->request->getContent(),true);
       $rules = [
         'ItemID' => 'required'
       ];

       $validator = Validator::make($input, $rules);
       if ($validator->fails()) {
           $errors = $validator->errors();
           $errorList = $this->checkErrors($rules, $errors);
           $additional = null;
           $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
           return response()->json($response);
       }
       $ItemID = @$input['ItemID'];
       $param = array('Status' => 'D','Archived' => now());
       $result = DB::table('Item')->where('ItemID', $ItemID)->update($param);

      $result = $this->checkReturn($result);

      return Response()->json($result);

  }

}
