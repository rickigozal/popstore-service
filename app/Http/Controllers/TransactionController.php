<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use DB;
use Validator;
use PDF;

class TransactionController extends Controller
{
    public function __construct(Request $request){
        $this->param = $this->checkToken($request);
        $this->request = $request;
    }

    public function getTransaction(){
      $result = DB::table('Transaction')
      ->leftjoin('Branch','Transaction.BranchID','=','Branch.BranchID')
      ->select(['TransactionID','Transaction.BranchID','BranchName','Date','GrandTotal'])
      ->orderby('Transaction.TransactionID','desc')
      ->get();
      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Transaction' => $result
      );

       return Response()->json($endresult);
    }

    public function getTransactionDetail(Request $request){

      $input = json_decode($this->request->getContent(),true);
      $rules = [
        'TransactionID' => 'required'
      ];

      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $TransactionID = $input['TransactionID'];
      $header = DB::table('Transaction')
      ->leftjoin('Branch','Transaction.BranchID','=','Branch.BranchID')
      ->select(['TransactionID','TransactionCode','Transaction.BranchID','BranchName','Date','GrandTotal','NewGrandTotal'])
      ->get();

      $item = DB::table('Transaction')
      ->leftjoin('TransactionDetail','Transaction.TransactionID','=','TransactionDetail.TransactionID')
      ->leftjoin('Item','Item.ItemID','=','TransactionDetail.ItemID')
      ->leftjoin('Category','Item.CategoryID','=','Category.CategoryID')
      ->leftjoin('Branch','Branch.BranchID','=','Transaction.BranchID')
      ->select(['TransactionDetail.ItemID','ItemCode','ItemName','TransactionDetail.Price','Quantity','SubTotal','Item.CategoryID','CategoryName'])
      ->where('Transaction.TransactionID',$TransactionID)
      ->get();

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'TransactionDetail' => array(
              'Header' => $header,
              'Item' => $item
          )
      );
      return Response()->json($endresult);
}

public function insertUpdateTransaction(Request $request){
    $input = json_decode($request->getContent(),true);
    $rules = [
        'BranchID' => 'required',
        'Date' => 'date|date_format:Y-m-d|nullable',
        'Item' => 'required'
    ];

    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $errors = $validator->errors();
        $errorList = $this->checkErrors($rules, $errors);
        $additional = null;
        $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
        return response()->json($response);
    }


    $paramheader = array(
        'BranchID' => $input['BranchID'],
        'Date' => @$input['Date']
    );
    // return $paramheader;
        //harus cek dlu di table stock, udah ada item itu belum.
    $result = DB::table('Transaction')
    ->insert($paramheader);
    $TransactionID = $this->getLastVal();
    $TransactionCode = "Trans".$TransactionID;
    $result = DB::table('Transaction')
    ->where('TransactionID',$TransactionID)
    ->update(array('TransactionCode' => $TransactionCode));
    // return $TransactionCode;

    //insert detail transaction nya.
    for($i = 0; $i < count($input['Item']); $i++){
        $item = $input['Item'][$i];


        $param = array(
            'TransactionID' => $TransactionID,
            'ItemID' => $item['ItemID'],
            'Quantity' => $item['Quantity'],
            'Price' => $item['Price'],
            'SubTotal' => $item['SubTotal']);

            $result = DB::table('TransactionDetail')
            ->insert($param);
            //setelah transaksi, hitung stock barang yang di beli, dan dikurang.
            $stock = DB::table('BranchStock')
            ->where('BranchID',$paramheader['BranchID'])
            ->where('ItemID',$item['ItemID'])
            ->first()->Quantity;

            $newstock = $stock - $param['Quantity'];

            $result = DB::table('BranchStock')
            ->where('BranchID',$paramheader['BranchID'])
            ->where('ItemID',$item['ItemID'])
            ->update(array('Quantity'=> $newstock));
    }

    $GrandTotal = DB::table('TransactionDetail')
    ->where('TransactionID',$TransactionID)
    ->sum('SubTotal');

    // $GrandTotal = $input['GrandTotal'];
    $NewGrandTotal = $input['NewGrandTotal'];
    $result = DB::table("Transaction")
    ->where('TransactionID',$TransactionID)
    ->update(array('GrandTotal' => $GrandTotal, 'NewGrandTotal' => $NewGrandTotal));
          $branchstock = DB::table('BranchStock')
          ->leftjoin('Item','Item.ItemID','=','BranchStock.ItemID')
          ->leftjoin('Category','Item.CategoryID','=','Category.CategoryID')
          ->select(['BranchStockID','BranchStock.ItemID','ItemCode','ItemName','Item.Description','Item.CategoryID','CategoryName','BranchStock.Quantity'])
          ->orderby('BranchStock.ItemID','desc')
          ->get();
          $result = $this->checkReturn($result);
          $result['Stock'] = $branchstock;
          return Response()->json($result);

  }

  public function DeleteItem(Request $request){
       $input = json_decode($this->request->getContent(),true);
       $rules = [
         'ItemID' => 'required'
       ];

       $validator = Validator::make($input, $rules);
       if ($validator->fails()) {
           $errors = $validator->errors();
           $errorList = $this->checkErrors($rules, $errors);
           $additional = null;
           $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
           return response()->json($response);
       }
       $ItemID = @$input['ItemID'];
       $param = array('Status' => 'D','Archived' => now());
       $result = DB::table('Item')->where('ItemID', $ItemID)->update($param);

      $result = $this->checkReturn($result);

      return Response()->json($result);

  }

}
