<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use DB;
use Validator;
use PDF;

class CategoryController extends Controller
{
    public function __construct(Request $request){
        $this->param = $this->checkToken($request);
        $this->request = $request;
    }

    public function getCategory(){
      $result = DB::table('Category')
      ->select(['CategoryID','CategoryCode','CategoryName','Description'])
      ->where('Status',null)
      ->orderby('CategoryID','desc')
      ->get();
      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Category' => $result
      );

       return Response()->json($endresult);
    }

    public function getCategoryDetail(Request $request){

      $input = json_decode($this->request->getContent(),true);
      $rules = [
        'CategoryID' => 'required'
      ];

      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $CategoryID = $input['CategoryID'];
      $result = DB::table('Category')
      ->select(['CategoryID','CategoryCode','CategoryName','Description'])
      ->where('CategoryID',$CategoryID)
      ->get();
      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Category' => $result
      );
      return Response()->json($endresult);
}

public function insertUpdateCategory(Request $request){
    $input = json_decode($request->getContent(),true);
    $rules = [
        'CategoryCode' => 'required',
        'CategoryName' => 'required'
    ];

    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $errors = $validator->errors();
        $errorList = $this->checkErrors($rules, $errors);
        $additional = null;
        $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
        return response()->json($response);
    }
    $ID = @$input['CategoryID'];
    $unique = array(
        'Table' => "Category",
        'ID' => $ID,
        'Column' => "CategoryName",
        'String' => $input['CategoryName']
    );
    $uniqueCategoryName = $this->unique($unique);
    $unique['Column'] = "CategoryCode";
    $unique['String'] = $input['CategoryCode'];
    $uniqueEmail = $this->unique($unique);
    $param = array(
        'CategoryCode' => $input['CategoryCode'],
        'CategoryName' => $input['CategoryName'],
        'Description' => @$input['Description']);

      if ($ID == null){
        $result = DB::table('Category')->insert($param);
        $ID = $this->getLastVal();
        }
      else{$result = DB::table('Category')->where('CategoryID',$ID)->update($param);}

          $result = $this->checkReturn($result);
          return Response()->json($result);

  }

  public function DeleteCategory(Request $request){
       $input = json_decode($this->request->getContent(),true);
       $rules = [
         'CategoryID' => 'required'
       ];

       $validator = Validator::make($input, $rules);
       if ($validator->fails()) {
           $errors = $validator->errors();
           $errorList = $this->checkErrors($rules, $errors);
           $additional = null;
           $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
           return response()->json($response);
       }
       $CategoryID = @$input['CategoryID'];
       $param = array('Status' => 'D','Archived' => now());
       $result = DB::table('Category')->where('CategoryID', $CategoryID)->update($param);

      $result = $this->checkReturn($result);

      return Response()->json($result);

  }

}
