<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});
Route::post('login','LoginController@login');

Route::post('get_transaction','TransactionController@getTransaction');
Route::post('get_transaction_detail','TransactionController@getTransactionDetail');
Route::post('insert_update_transaction','TransactionController@insertUpdateTransaction');

Route::post('get_category','CategoryController@getCategory');
Route::post('insert_update_category','CategoryController@insertUpdateCategory');
Route::post('delete_category','CategoryController@DeleteCategory');

Route::post('get_item','ItemController@getItem');

Route::post('get_branch_stock','BranchStockController@getBranchStock');
Route::post('insert_update_branch_stock','BranchStockController@insertUpdateBranchStock');
